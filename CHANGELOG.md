# Blender Asset Tracer changelog

This file logs the changes that are actually interesting to users (new features,
changed functionality, fixed bugs).

## Version 1.0 (in development)

- Base version after which changes will be recorded here.
